import socket
import sys

from PyQt4 import QtCore, QtGui

from PyQt4.QtCore import QThread,SIGNAL

from ReceiverGUI import Ui_MainWindow


UDP_IP = "127.0.0.1"
UDP_PORT = 5005
 
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.settimeout(1)
sock.bind((UDP_IP, UDP_PORT))


class receivingThread(QThread):
    
    def __init__(self):
        QThread.__init__(self)
        
    def __del__(self):
        self.wait();
    
    def _receive(self):            
            while True:
                try:
                    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
                    self.emit(SIGNAL('received(QString)'), data)
                except:
                    print ""
    def run(self):
        self._receive()
        self.sleep(2)

class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.SendButton.clicked.connect(self.sendBtnClicked)
        self.get_thread = receivingThread()
        self.connect(self.get_thread, SIGNAL("received(QString)"), self.received)
    
    def received(self,data):
        self.ui.listView.addItem(data)
        self.ui.listView.scrollToBottom()      
    
        
    def sendBtnClicked(self):  
        if self.get_thread.isRunning():
            self.get_thread.terminate()
            self.ui.SendButton.setText("Start Receiving")
        else:
            self.get_thread.start()
            self.ui.SendButton.setText("Stop Receiving")
                       
            

        

def run():
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())

    
run()    
